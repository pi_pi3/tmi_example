#define _GNU_SOURCE 1
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include "tmi_cxx.h"

typedef struct Command {
    char *command;
    char *response;
} Command;

typedef struct Userdata {
    FILE *chat_log;
    char **greetings;
    Command *commands;
} Userdata;

int userdata_greetings_count(Userdata *userdata) {
    int i = 0;
    while (userdata->greetings[i]) {
        i++;
    }
    return i;
}

void catch_err(TmiClient *client, TmiObject *reason) {
    fprintf(stderr, "* non-fatal error\n");
}

ssize_t tmi_snprintf(char *buf, size_t n, char *_fmt, TmiClient *client, char *channel, TmiObject *userstate, char *msg) {
    char *fmt = malloc(strlen(_fmt) + 1);
    char *fmt_ptr = fmt;
    strcpy(fmt, _fmt);

    size_t i = 0;
    while (*fmt) {
        if (*fmt == '$') {
            ++fmt;
            if (*fmt != '(') {
                fprintf(stderr, "invalid tmi_snprintf format\n");
                return -1;
            }
            ++fmt;

            char *key = fmt;

            while (*fmt != ')') {
                if (!*fmt) {
                    fprintf(stderr, "invalid tmi_snprintf format\n");
                    return -1;
                }
                ++fmt;
            }
            *fmt = 0;

            if (strcmp(key, "channel") == 0) {
                strcpy(buf + i, channel);
                i += strlen(channel);
            } else if (strcmp(key, "display-name") == 0 || strcmp(key, "username") == 0) {
                TmiObject *value_obj = tmi_object_get(userstate, key);
                char *value = tmi_object_to_string(value_obj);

                if (value) {
                    strcpy(buf + i, value);
                    i += strlen(value);
                    free(value);
                } else {
                    char *value = "(null)";
                    strcpy(buf + i, value);
                    i += strlen(value);
                }

                tmi_del_object(value_obj);
            }
        } else {
            buf[i] = *fmt;
            ++fmt;
            ++i;
        }
    }

    free(fmt_ptr);

    buf[i] = 0;

    return i;
}

void command(TmiClient *client, char *channel, TmiObject *userstate, char *msg) {
    Userdata *userdata = tmi_userdata(client);
    Command *command = userdata->commands;
    while (command->command) {
        if (strstr(msg, command->command) == msg) {
            char *buf = malloc(2048);
            tmi_snprintf(buf, 2048, command->response, client, channel, userstate, msg + strlen(command->command) + 1);

            TmiPromise *hi_promise = tmi_client_say(client, channel, buf);
            tmi_promise_or_else(hi_promise, catch_err);

            free(buf);
            break;
        }

        ++command;
    }
}

void greeting(TmiClient *client, char *channel, TmiObject *userstate, char *msg) {
    Userdata *userdata = tmi_userdata(client);
    char *greeting = userdata->greetings[rand() % userdata_greetings_count(userdata)];

    char *buf = malloc(2048);
    tmi_snprintf(buf, 2048, greeting, client, channel, userstate, "");

    TmiPromise *hi_promise = tmi_client_say(client, channel, buf);
    tmi_promise_or_else(hi_promise, catch_err);

    free(buf);
}

void on_message(TmiClient *client, char *channel, TmiObject *userstate, char *msg, int self) {
    Userdata *userdata = tmi_userdata(client);

    TmiObject *display_name_obj = tmi_object_get(userstate, "display-name");
    char *display_name = tmi_object_to_string(display_name_obj);

    if (display_name) {
        fprintf(userdata->chat_log, "%s: %s\n", display_name, msg);
        fflush(userdata->chat_log);
        free(display_name);
    } else {
        fprintf(userdata->chat_log, "(null): %s\n", display_name, msg);
        fflush(userdata->chat_log);
    }

    tmi_del_object(display_name_obj);

    if (self) {
        return;
    }

    if (msg[0] == '!') {
        command(client, channel, userstate, msg + 1);
    } else if (strcasestr(msg, "walterpot")) {
        greeting(client, channel, userstate, msg);
    }
}


static char *greetings[] = {
    "Hi, $(display-name)",
    "Hello there!",
    NULL
};

static Command commands[] = {
    {"project", "tmi_cxx"},
    {"bot", "I am WalterBot, beep boop."},
    {"help", "!project, !bot, !help"},
    {NULL, NULL}
};

void tmicxx_main(TmiClient *client) {
    srand(time(NULL));

    Userdata *userdata = malloc(sizeof(Userdata));
    userdata->chat_log = fopen("../chat.log", "w");
    userdata->greetings = greetings;
    userdata->commands = commands;

    tmi_connect(client, userdata);
    tmi_on_message(client, on_message);
}
