.PHONY: run

run: tmi_test.so
	cd tmi_cxx; node tmi_cxx.js ../tmi_test.so ../config.json  ../secret.json

tmi_test.so: test.c tmi_cxx/build/Release/tmi_cxx.node
	cc -Itmi_cxx/include -shared -fPIC -o tmi_test.so test.c tmi_cxx/build/Release/tmi_cxx.node

tmi_cxx/build:
	cd tmi_cxx; npm install tmi.js
	cd tmi_cxx; npm install node-addon-api
	cd tmi_cxx; node-gyp configure

tmi_cxx/build/Release/tmi_cxx.node: tmi_cxx/build
	cd tmi_cxx; node-gyp build
